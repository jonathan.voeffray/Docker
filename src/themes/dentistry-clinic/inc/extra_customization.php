<?php 

$dental_insight_custom_style= "";

// Slider-content-alignment

$dental_insight_slider_content_alignment = get_theme_mod( 'dental_insight_slider_content_alignment','LEFT-ALIGN');

if($dental_insight_slider_content_alignment == 'LEFT-ALIGN'){

$dental_insight_custom_style .='.carousel-caption{';

	$dental_insight_custom_style .='text-align:left; right: 45%; left: 15%;';

$dental_insight_custom_style .='}';


}else if($dental_insight_slider_content_alignment == 'CENTER-ALIGN'){

$dental_insight_custom_style .='.carousel-caption{';

	$dental_insight_custom_style .='text-align:center; right: 15%; left: 15%;';

$dental_insight_custom_style .='}';


}else if($dental_insight_slider_content_alignment == 'RIGHT-ALIGN'){

$dental_insight_custom_style .='.carousel-caption{';

	$dental_insight_custom_style .='text-align:right; right: 15%; left: 45%;';

$dental_insight_custom_style .='}';

}
// theme-button

$dentistry_clinic_theme_button_color = get_theme_mod('dentistry_clinic_theme_button_color','#00bcd5');

if($dentistry_clinic_theme_button_color != false){

$dental_insight_custom_style .='button, input[type="button"] ,input[type="submit"],.home-btn a, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,button.search-submit, .toggle-menu button, a.more-link, .prev.page-numbers, .next.page-numbers, .site-footer .search-form .search-submit,a.added_to_cart.wc-forward, .box-btn a{';

$dental_insight_custom_style .='background-color: '.esc_attr($dentistry_clinic_theme_button_color).';';

$dental_insight_custom_style .='}';
}
$dental_insight_button_border = get_theme_mod('dental_insight_button_border_radius','30');

if($dental_insight_button_border != false){

$dental_insight_custom_style .='button, input[type="button"] ,input[type="submit"],.home-btn a, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,button.search-submit, .toggle-menu button, a.more-link, .prev.page-numbers, .next.page-numbers, .site-footer input[type="search"],#sidebar input[type="search"], input[type="search"],a.added_to_cart.wc-forward, .box-btn a{';

$dental_insight_custom_style .='border-radius: '.esc_attr(

$dental_insight_button_border).'px;';

$dental_insight_custom_style .='}';
}